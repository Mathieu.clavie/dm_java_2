import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer res = null;
        if (liste.isEmpty()){
            return null;
        }
        else{
            res = liste.get(0);
            for (int elem : liste){
                if(elem<res){
                    res = elem;
                }
            }
        }
        return res;
        
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T val : liste){
            if (valeur.compareTo(val)>=0){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        Set<T> ensemble1 = new HashSet<>(liste1);
        Set<T> ensemble2 = new HashSet<>(liste2);
        ensemble2.retainAll(ensemble1);
        List<T> listeintersect = new ArrayList<>(ensemble2);
        Collections.sort(listeintersect);
        return listeintersect;
    }

    public static ArrayList<String> separe(String texte, String sep){
        ArrayList<String> liste = new ArrayList<>();
        String[] res = texte.trim().split(sep);
        for (String elem : res){
                liste.add(elem);            
        }
        return liste;
    }
    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        return separe(texte, " ");
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        int val = 0;
        String res = null;
        List<String> liste = decoupe(texte);
        for (String mot : liste){
            if ((Collections.frequency(liste, mot))>val){
                res = mot;
                val = Collections.frequency(liste, mot);
            }
            else if((Collections.frequency(liste, mot)) == val){
                if (mot.compareTo(res)<0){
                    res = mot;
                }                
            }
        }
        return res;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        ArrayList<String> liste = separe(chaine,"");
        int parouv = Collections.frequency(liste, "(");
        int parfer = Collections.frequency(liste, ")");
        if (!(parouv==parfer)){
            return false;
        }
        else{
            if ((liste.get(0).equals(")")) || (liste.get(liste.size()-1).equals("("))){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        ArrayList<String> liste = separe(chaine,"");
        int parouv = Collections.frequency(liste, "(");
        int parfer = Collections.frequency(liste, ")");
        int croouv = Collections.frequency(liste, "[");
        int crofer = Collections.frequency(liste, "]");
        if (!(parouv==parfer && croouv == crofer)){
            return false;
        }
        else{
            if ((liste.get(0).equals(")")) || (liste.get(liste.size()-1).equals("(")) || (liste.get(0).equals("]")) || (liste.get(liste.size()-1).equals("["))){
                return false;
            }
            for (int i = 0; i<liste.size()-1;i++){
                if (liste.get(i).equals("[") && liste.get(i+1).equals(")")){
                    return false;
                }
                else if (liste.get(i).equals("(") && liste.get(i+1).equals("]")){
                    return false;
                }
            }
        }
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        Integer milieu = 0;
        Integer deb = 0;
        Integer fin = liste.size();
        boolean fini = false;
        while(!fini && ((fin - deb) > 1)){
          milieu = (deb + fin)/2;
          fini = (liste.get(milieu)==valeur || liste.get(milieu-1)==valeur);
          if(liste.get(milieu)>valeur){
            fin = milieu;
          }
          else{
            deb = milieu;
          }
        }
        return fini;
    }


}
